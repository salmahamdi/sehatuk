var request = require('request');
var fs = require('fs');
var file = 'bts.txt';
var base_url= "http://www.anam.ma/wp-admin/admin-ajax.php?action=get_Medicament&term=";

var options = {
  headers: {'user-agent': 'node.js'}
}


function get_alphabet(){ 
	var alphabet = []; 
	for(i=97;i<123;i++) 
		alphabet.push(String.fromCharCode(i)); 
	return alphabet;
}

function get_url(alphabet){
	var urls=[];
	for(var i=0;i<alphabet.length;i++)
		urls.push(base_url+alphabet[i]);
	return urls;
}

function names_array(body,names){
	for(var j=0;j<body.length;j++)
		names.push(body[j].value);
}

function write_file(file,content){
	fs.writeFile(file,content,function(err) {
		if(err) 
			return console.error(err);
    });
}

function get_names(){
	var names=[];
	var urls=get_url(get_alphabet());
	for(var i=0;i<urls.length;i++){
		request(urls[i], options,function (error, response, html) {
 			if (!error)  {
 				if(response.statusCode==200){
 					names_array(JSON.parse(response.body),names);
					write_file(file,JSON.parse(JSON.stringify(names))); 
 				}		
			}
		});
	}
}	

get_names();


