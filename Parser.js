var cheerio = require('cheerio'),
	cheerioTableparser = require('cheerio-tableparser');
var request = require('request');
var nano= require('nano')('http://localhost:5984');
var fs = require('fs');
var file = 'bts.txt'
var file_out = 'output.json'
var test_db = nano.db.use('medicaments');
var options = {
  headers: {'user-agent': 'node.js'}
}

function read_file(file){
	try {  
    var data = fs.readFileSync(file, 'utf8');
    data=data.split(',');
    } catch(e) {
    console.log('Error:', e.stack);}

    return(data);
}
function write_file(file_out,content){
	fs.appendFile(file_out,content,function(err) {
		if(err) 
			return console.error(err);}
		);


}

function get_urls(file){
	var urls=[];
	var names=read_file(file);
	for(var i=0;i<names.length;i++)
		urls.push('http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/?tags='+names[i]+'&search=type1/');
	return urls;
}

function  get_data(html){
	var $ = cheerio.load(html);
	cheerioTableparser($);
	var data=$('.list-medicament').parsetable(false, false, true);
	return data;
}

function format_data(data){
	if(data[1]==undefined){
	for(var i=0;i<11;i++)
		data[i]=String(data[i]).split(',');}
	return data;
}

function info_object(data,j){
	var info={
		'Code': data[0][j],
		'Medicament': data[1][j],
		'Substance active (DCI) & dosage': data[2][j] ,
		'Forme & Presentation': data[3][j],
		'Prix': data[4][j],
		'Public de Vente (*PPV)': data[5][j],
		'Prix base remboursement (PPV)': data[6][j],
		'Prix Hospitalier (**PH )': data[7][j],
		'Classe Therapeutique': data[8][j] ,
		'P:Princeps G:Generique': data[9][j],
		'Remboursement': data[10][j]
			};
	return info;
}
function insert_db(info){
	if(info['Code']==""){
		id=info.Medicament+" " +info['Forme & Presentation']+" "+info['Substance active (DCI) & dosage'];
	}
	else
		id=info['Code'];
	test_db.insert(info, id, function(err, body){
 	 if(!err){
    	console.log(err);
  }
});
}



var partitionedRequests = (function () {
    var queue = [], running = 0,k=0;
    function sendPossibleRequests() {
        var url;
        while (queue.length > 0 && running < 5) {
            url = queue.shift();
            running++;
            request(url, options, function (err, res, html) {
                running--;
                sendPossibleRequests();

                if(err)
                    console.log(err);
                else {
                    var data=get_data(html);
                    console.log(data);
 					data=format_data(data);
 					
 					if(data[0].length>1){
 					 					for(var j=1;j<data[0].length;j++)
 					 					{	var info=info_object(data,j);
 					 					 	write_file(file_out,JSON.stringify(info));
 					 					 	insert_db(info);
 					 					 }}
 					if(data[0].length>10){
 						url_page2=url;
 						url_page2.replace('http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/','http://www.anam.ma/regulation/guide-medicaments/recherche-de-medicaments-par-nom/page/2/');
 						queue.push(url_page2);					}
                }
            });
        }
    }
   
    return function (url) {
     	queue.push(url); 	      
    	sendPossibleRequests();
    };
})();


var urls=get_urls(file);

for(var k=0;k<urls.length;k++)
	partitionedRequests(urls[k]);