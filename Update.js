var request = require('request');
var nano= require('nano')('http://localhost:5984');
var fs = require('fs');
var nodemailer = require('nodemailer');
var async= require('async');
var file = 'output.json';
var Promise= require('promise');
var file_out='changes.json';
var db = nano.db.use('medicaments'), per_page = 100
  , params   = {include_docs: true, descending: true};
var doc_url='http://localhost:5984/_utils/document.html?medicaments/';
var changes=[];

//remmeber to call parser
var database=[];
function format_data(data){
	var data_formatted=data.replace(/}{/g,'},{');
	data_formatted='['+data_formatted+']';
	data_formatted=JSON.parse(data_formatted);
	return data_formatted;
}
function read_file(file){
	try {  
    var data = fs.readFileSync(file, 'utf8');
   	data=format_data(data);
   	console.log(data.length);
    } catch(e) {
    console.log('Error:', e.stack);}

    return data;
}

function get_docs(data){
	var database=[];
	db.list(params, function(error,body,headers) {
		
		for(var i=0;i<body.total_rows;i++){
			
			var doc=body.rows[i].doc;
			database.push(doc);
	}
	//console.log(database);
	console.log(database.length);
  	compare_elements(data,database);
  	
	return database;
	});

}

function get_key(elem){

	var key= String(elem.Medicament+" " +elem['Forme & Presentation']+" "+elem['Substance active (DCI) & dosage']);
	//var key=elem.Code;
	//console.log(key);
	return key;
}



function update(old_elem,new_elem){
	console.log(old_elem);
	db.destroy(old_elem._id,old_elem._rev,function(err, body) {

  if (!err){
  	 console.log("here rrrrrrrrr");
    console.log(body);
	insert_elem(new_elem);
		}
		else{ console.log("here errrrrrrrrrrrrrr");
					console.log(err);}
	
});
	
}

function difference(old_elem,new_elem){

	var flag=0;
	if(old_elem['Prix']!==new_elem['Prix']){
		flag=1;
	}
	
	if(old_elem['Public de Vente (*PPV)']!==new_elem['Public de Vente (*PPV)']){
		flag=1;
	}
	if(old_elem['Prix base remboursement (PPV)']!==new_elem['Prix base remboursement (PPV)']){
		flag=1;
	}
	if(old_elem['Prix Hospitalier (**PH )']!==new_elem['Prix Hospitalier (**PH )']){
		flag=1;
	}
	if(old_elem['Classe Therapeutique']!==new_elem['Classe Therapeutique']){
		flag=1;
	}
	if(old_elem['P:Princeps G:Generique']!==new_elem['P:Princeps G:Generique']){
		flag=1;
	}
	
	if(flag==1){
		update(old_elem,new_elem);
	}

}

function write_file(file_out,content){
	fs.appendFile(file_out,content,function(err) {
		if(err) 
			return console.error(err);}
		);


}

function get_id(elem){
	if(elem.Code=="")
		var id=elem.Medicament+" " +elem['Forme & Presentation']+" "+elem['Substance active (DCI) & dosage'];
	
	else
		var id=elem['Code'];
	return id;
}

function insert_elem(elem){
	var id=get_id(elem);
	db.insert(elem, elem['Code'], function(err, body){
		
 	 if(!err){
    	console.log("here2");
    	var content=doc_url+id;
		changes.push(content);
		write_file(file_out,elem.Medicament+": "+
			content+",");
  }

});
}

function find_elem(database,elem){
	var flag=0;
	//console.log(elem);
	var key=get_key(elem);
	
	for(var i=0;i<database.length;i++){
		var temp_key=get_key(database[i]);
		if(temp_key===key){
			flag=1;
			break;
		}
	}

	if (flag==1){
		console.log(key+'****'+temp_key);
		difference(database[i],elem);
	}
	else{

		console.log(key+'****'+temp_key);
		insert_elem(elem);
				}
		
}

function find_non_rembourssable_elems(elem,new_data){

	var key=get_key(elem);
	var flag=0;
	for(var i=0;i<new_data.length;i++){
		var temp_key=get_key(new_data[i]);
		if(key==temp_key){
			flag=1;
			break;}

	}
			if(flag==0 && elem['Remboursement']=='Remboursable' ){
			console.log(key);
			var new_elem={Code: elem.Code,
				'Medicament': elem['Medicament'],
				'Substance active (DCI) & dosage': elem['Substance active (DCI) & dosage'] ,
				'Forme & Presentation': elem['Forme & Presentation'],
				'Prix': elem['Prix'],
				'Public de Vente (*PPV)': elem['Public de Vente (*PPV)'],
				'Prix base remboursement (PPV)': elem['Prix base remboursement (PPV)'],
				'Prix Hospitalier (**PH )': elem['Prix Hospitalier (**PH )'],
				'Classe Therapeutique': elem['Classe Therapeutique'],
				'P:Princeps G:Generique': elem['P:Princeps G:Generique'],
				'Remboursement': 'Non-Remboursable'};
		
			update(elem,new_elem);}

}

function compare_elements(data,database){
	var m=0;
	//database:old data data:new data

		for(var i=0;i<data.length;i++){
			find_elem(database,data[i]);
	}
		//console.log(i);
		for(var j=0;j<database.length;j++){
			find_non_rembourssable_elems(database[j],data);
	}

	
}



function get_changes(){
	var string="";
	console.log("changes "+changes);

	for(var i=0;i<changes.length;i++){
		string+=changes[i]+"\n";
	}
	console.log(string);
	send_email(string);
}

function send_email(string){
	var transporter = nodemailer.createTransport({
	    host: 'smtp.gmail.com',
	    port: 465,
	    secure: true, // secure:true for port 465, secure:false for port 587
	    auth: {
	        user: 's.hamdi@dialy.net',
	        pass: '***'
	    }
	});
	//console.log('hi');
	var mailOptions = {
	  from: 's.hamdi@dialy.net',
	  to: 'salmahamdiofficial@gmail.com',
	  subject: 'Sending Email using Node.js',
	  text: string
	};
	//console.log(transporter);

	transporter.sendMail(mailOptions, function(error, info){
	  if (error) {
	    console.log(error);
	  } else {
	    console.log('Email sent: ' + info.response);
	  }
	}); }


function compare(){
	var data=read_file(file);

	get_docs(data);
	setTimeout(function(){ // simulate long running code - 3 seconds
       get_changes();
    },60000);
	
	//async.waterfall([get_docs(),get_changes()]);
	/*var changes_done=0;
	for(var i=0;i<2;i++){
		if(changes_done==0)
		{get_docs(data);
			changes_done++;}
		else if(changes_done==1){
			get_changes();
		}
	}*/
	}
compare();



//asyncFunc().then(return get_changes());
/*
 
var promise = new Promise(function (resolve, reject) {
	var data=read_file(file);
	get_docs(data);
 resolve();
 
  }).then(get_changes());*/

//new Promise(compare()).


